module.exports = {
    'extends': [
      '@beepbeepgo/eslint-config-beepbeepgo-common'
    ],
    rules: {
      'complexity': ['error', 15],
      'react/jsx-uses-vars': 1
    }
};
